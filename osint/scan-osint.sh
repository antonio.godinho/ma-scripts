#!/bin/bash

CWD=$(dirname $0)

function scan () {
	for site in $(find $CWD/$1/* -type d); do
		KEY=$(cat $site/KEY.txt)
		for file in $site/*.command; do
			printf "\n------------------------------------------------------------------------------"
			printf "\n * $file"
			printf "\n------------------------------------------------------------------------------\n"
			[ -e "$file" ] || continue
			API_CALL=$(sed -e "s/\__SEARCH__/$2/" "$file" -e "s/\__KEY__/$KEY/" -e "s/\__CWD__/"\${site}"/")
			eval $API_CALL
		done
	done
}

if [[ "$#" -ne 2  ]]; then
	printf "*** ERROR:$0 - takes 2 parameters, criteria flag [ domain | hostname | ip | sha | apt | year ] and search value...\n"
	exit 1
else
	scan $1 $2
fi
