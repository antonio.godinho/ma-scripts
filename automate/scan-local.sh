#!/bin/bash

CWD=$(dirname $0)

function scan () {
    printf "\nFILE INFO --------------------------------------------------------------------\n\n"
    "$CWD"/engine/file/scan-file.sh $1
    printf "\nYARA RULES -------------------------------------------------------------------\n\n"    
    "$CWD"/engine/yara/scan-yara.sh $1
    printf "\nANTI-VIRUS -------------------------------------------------------------------\n\n"    
    "$CWD"/engine/av/scan-av.sh $1
}

if [[ "$#" -ne 1  ]]; then
    printf "*** ERROR:$0 - missing scan file...\n"
    exit 1
else
    scan $1
fi