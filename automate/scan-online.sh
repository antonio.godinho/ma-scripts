#!/bin/bash

CWD=$(dirname $0)

function scan () {
    printf "\nOSINT ------------------------------------------------------------------------\n"
    sha256sum $1 | cut -d " " -f1 | xargs -I@ "$CWD"/engine/osint/scan-osint.sh sha @
}

if [[ "$#" -ne 1  ]]; then
    printf "*** ERROR:$0 - missing scan file...\n"
    exit 1
else
    scan $1
fi