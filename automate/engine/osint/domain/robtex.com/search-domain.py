#!/usr/bin/env python3

import sys
from selenium import webdriver
from webdriver_manager.firefox import GeckoDriverManager
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.firefox.options import Options

if len(sys.argv) != 2:
    raise ValueError("*** ERROR:" + sys.argv[0] + " - missing search criteria...")

__SEARCH__ = sys.argv[1]

opts = Options()
opts.headless = True
driver = webdriver.Firefox(executable_path=GeckoDriverManager().install(), options=opts, service_log_path="/dev/null")


# Scraper
# -------------------------------------------------------------------------------

driver.get("https://www.robtex.com")
search_form = WebDriverWait(driver, 20).until(EC.visibility_of_element_located((By.ID, "q")))
search_form.send_keys(__SEARCH__)
search_form.submit()

# WRONG, JUST A POC AT THE MOMENT
#
#
#
print(driver.page_source)

driver.quit()
