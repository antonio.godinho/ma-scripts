#!/bin/bash

RULES=$(dirname $0)"/rules"

function scan () {
  find ${RULES} -name "*.yar*" | xargs -I@ yara @ $1 2>/dev/null | cut -d " " -f1 | sort | uniq
}

if [[ "$#" -ne 1  ]]; then
  printf "*** ERROR:$0 - takes 1 parameter, file to scan...\n"
  exit 1
else
  scan $1
fi
