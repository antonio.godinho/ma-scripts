#!/bin/bash

CWD=$(dirname $0)
CWD_ABSOLUTE=$(pwd)
DIRECTORIES=($(find "$CWD" -maxdepth 2 -mindepth 1 -type d))

function update () {
    for directory in "${DIRECTORIES[@]}"; do
        if [ ${#DIRECTORIES[@]} -gt 0 ]; then
            printf "Checking directory: $directory ... "
            cd $directory
            if git rev-parse --git-dir > /dev/null 2>&1; then
                printf "git repository found, updating... "
                git pull origin master
            else
                printf "not a git repository!\n"
            fi
            cd "$CWD_ABSOLUTE"
        else
            printf "No directories to check, exiting ..."
        fi
    done
}

update