#!/bin/bash

function scan () {
    printf "FILE                            : "
    file --brief --preserve-date $1
    printf "MD5                             : "
    md5sum $1 | cut -d " " -f1
    printf "SHA256                          : "
    sha256sum $1 | cut -d " " -f1
    printf "SSDEEP                          : "
    ssdeep -b $1 | tail -n +2
    printf "\nEXIFTOOL ---------------------------------------------------------------------\n"
    exiftool $1
    printf "\nFLOSS ------------------------------------------------------------------------\n"
    /lab/tools/floss/floss $1
    printf "\nOLEVBA -----------------------------------------------------------------------\n"
    olevba --analysis --json $1 | jq
    printf "\nPORTEX -----------------------------------------------------------------------\n"
    java -jar /lab/tools/portex/portex_2.10-2.0.9_FAT.jar $1
}

if [[ "$#" -ne 1  ]]; then
    printf "*** ERROR:$0 - takes 1 parameter, file to scan...\n"
    exit 1
else
    scan $1
fi