#!/bin/bash

CWD=$(dirname $0)
DIRECTORY_EXEC="/lab/tools/f-prot"

function scan () {
    printf "Running F-PROT AV... \n"
		"$DIRECTORY_EXEC"/fpscan --report --verbose=0 --nospin $1
}

if [[ "$#" -ne 1  ]]; then
    printf "*** ERROR:$0 - takes 1 parameter: file to scan...\n"
    exit 1
else
    scan $1
fi
