#!/bin/bash

CWD=$(dirname $0)
DATABASE_DIRECTORY="$CWD/db"
LATEST=$(find $DATABASE_DIRECTORY -type d -printf "%f\n" | tail -n 1)
DATABASE="$DATABASE_DIRECTORY/$LATEST/clamav-database.cvd"

function scan () {
    printf "Running Clam AV... "
    if [[ $(find "$DATABASE_DIRECTORY" -type d -printf "%f\n" | tail -n 1 | wc -l) -eq 1 ]]; then
        printf "using database: $DATABASE\n"
        clamscan --no-summary --database $DATABASE $1
    else
        printf "no local database found in $DATABASE_DIRECTORY directory (run update-av-db.sh to download the latest version), using *default* database\n"
        clamscan --no-summary $1
    fi
}

if [[ "$#" -ne 1  ]]; then
    printf "*** ERROR:$0 - takes 1 parameter: file to scan...\n"
    exit 1
else
    scan $1
fi
