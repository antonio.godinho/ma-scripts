#!/bin/bash

NOW=$(date +"%Y%m%d%H%M%S")
CWD=$(dirname $0)

DB_FILENAME="clamav-database.cvd"
DATABASE_DIRECTORY="$CWD/db"
LATEST=$(find "$DATABASE_DIRECTORY" -type d -printf "%f\n" | tail -n 1)
DATABASE="$DATABASE_DIRECTORY/$LATEST/$DB_FILENAME"
DATABASE_WRITE_DIRECTORY="$DATABASE_DIRECTORY/$NOW"
SHA_ONE=$(sha1sum "$DATABASE" | cut -d " " -f1)

function update () {
    printf "Latest available AV database: $DATABASE with SHA: $SHA_ONE\n"
    mkdir "$DATABASE_WRITE_DIRECTORY"
    wget http://database.clamav.net/main.cvd --output-document="$DATABASE_WRITE_DIRECTORY/$DB_FILENAME"
    SHA_TWO=$(sha1sum "$DATABASE_WRITE_DIRECTORY/$DB_FILENAME" | cut -d " " -f1)
    printf "New file has SHA: $SHA_TWO"

    if [[ "$SHA_ONE" != "$SHA_TWO" ]]; then
        printf " - Updated AV database file was saved in directory $DATABASE_WRITE_DIRECTORY...\n"
    else
        printf " - Not different from existing AV database, removing $DATABASE_WRITE_DIRECTORY/$DB_FILENAME...\n"
        rm "$DATABASE_WRITE_DIRECTORY/$DB_FILENAME"
        rmdir "$DATABASE_WRITE_DIRECTORY"
    fi
}

update