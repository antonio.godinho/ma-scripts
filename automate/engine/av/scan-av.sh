#!/bin/bash

CWD=$(dirname $0)
EXEC="run-av.sh"

function scan () {
    for directory in $(find ${CWD} -mindepth 1 -maxdepth 1 -type d); do
        [ -e "$directory" ] || continue
        eval "${directory}/${EXEC} $1"
    done
}

if [[ "$#" -ne 1  ]]; then
    printf "*** ERROR:$0 - takes 1 parameter, file to scan...\n"
    exit 1
else
    scan $1
fi