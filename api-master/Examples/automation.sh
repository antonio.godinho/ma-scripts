#! /bin/bash

PATHS=(
    "chroniclingamerica.loc.gov" 
    "mercadobitcoin.net" 
)

for site in ${PATHS[@]}; do
    python3 ../api-master.py -u ./${site}/url.txt -e ./${site}/endpoint.txt -p ./${site}/parameters.json -o ./results-${site}.txt
done
