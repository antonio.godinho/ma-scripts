#!/usr/bin/env python3

import argparse
import signal
import sys
import json
import os

try:
    from urllib.parse import urljoin
    from requests import get, post
    from requests.exceptions import HTTPError
except ImportError as err:
    print(f"Failed to import required modules: {err}")


APP_VERSION = "20200629"
APP_NAME = "Terminal API parser"

signal.signal(signal.SIGINT, signal.SIG_DFL)

def main(url, endpoint, parameters, output, silent):
    if not silent:
        print("\n*** To run in silent mode, use -s ***\n")
        print("URL: " + str(url))
        print("Endpoint: " + str(endpoint))
        print("Parameters: " + str(json.loads(parameters)))
        print("Output: " + str(output))
        print("API: " + str(urljoin(url, endpoint)))
        print(
            "\n---[ BEGIN ]------------------------------------------------------------\n")

    try:
        response = get(urljoin(url, endpoint), timeout=15, params=json.loads(parameters))
        response.raise_for_status()
        data = response.json()
        jstr = json.dumps(data, ensure_ascii=False, indent=4)

        if output != "STDOUT":
            with open(os.path.join(output), "w") as output_file:
                output_file.write(jstr)
            print("Data was saved to " + os.path.join(output))
        else:
            print(jstr)

    except HTTPError as http_err:
        print(f'HTTP error occurred: {http_err}')
    except Exception as err:
        print(f'Error occurred: {err}')

    if not silent:
        print(
            "\n--------------------------------------------------------------[ END ]---\n")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description=APP_NAME + ": A simple tool written by Antonio Godinho (ctrlc@tutanota.com) to help ease the process of using APIs from the terminal")
    parser_input = parser.add_argument_group("input named arguments")
    parser_input.add_argument(
        "-u", "--url", help="URL or file with URL to use", dest="url", required=True)
    parser_input.add_argument(
        "-e", "--endpoint", help="API endpoint or file with endpoint to use", dest="endpoint", required=True)
    parser_input.add_argument(
        "-p", "--parameters", help="Parameters (in valid JSON format) to be passed to the request or file containing the parameters", dest="parameters", required=True)
    parser_output = parser.add_argument_group("output named arguments")
    parser_output.add_argument(
        "-o", "--output", help="Saves the returned data into a file", default="STDOUT", dest="output")
    parser_general = parser.add_argument_group("general named arguments")
    parser_general.add_argument("-s", "--silent", action="store_true",
                                help="Runs in silent mode, no output to STDOUT other than the returned data", dest="silent")
    parser_general.add_argument("-v", "--version",
                                action="version", version=APP_VERSION)

    arguments = parser.parse_args()

    if os.path.exists(arguments.url):
        with open(arguments.url, "r") as data_file:
            url = data_file.readline().strip()
        arguments.url = url

    if os.path.exists(arguments.endpoint):
        with open(arguments.endpoint, "r") as data_file:
            endpoint = data_file.readline().strip()
        arguments.endpoint = endpoint

    if os.path.exists(arguments.parameters):
        with open(arguments.parameters, "r") as data_file:
            if os.stat(arguments.parameters).st_size != 0:
                parameters = data_file.read()
            else:
                parameters = None
        arguments.parameters = parameters

    sys.exit(main(arguments.url, arguments.endpoint,
                  arguments.parameters, arguments.output, arguments.silent))
