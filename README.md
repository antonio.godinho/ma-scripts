# ma-scripts

Various scripts:
* **api-master.py** : Retrieves information from a given API, the API URL, Endpoint, and Parameters (in JSON format) can be passed from the command line or from a file (See Examples folder for how to automate the process). **NOTE:** Pipping values into the input parameters is possible although not using the traditional | instead you can pipe using `` `COMMAND` `` for instance: *./api-master.py -u `` `cat url.txt` `` -e search/pages/results/ -p ./Examples/chroniclingamerica.loc.gov/parameters.json -o ./pipe-example.txt*

* **scan-osint.sh** : Retrieves information from various websites, the script takes 2 arguments. The first argument is the search criteria with valid values of "domain", "hostname", "ip", "sha", "apt" and "year"; the last parameter is the search value. For instance "scan-osint.sh year 2017" should display a list of reports for the year 2017 **NOTE 1:** For any websites which require a Key, you must add your key into the KEY.TXT file **NOTE 2:** A Python 3 PoC Selenium scraper is included in folder /domain/robtex.com although a PoC you shouldn't get any problems getting it running "search-domain.py"

* **automate** : Directory contains my lab automation, this is and will be a work in progress! **NOTE:** This directory contains scan-osint.sh, for any websites which require a Key, you must add your key into a KEY.TXT file

* scan-local.sh : scan a file using local tools
* scan-online.sh : scan a file for online OSINT information

**ENGINE:**
* av : antivirus related scripts
* file : file analysis related scripts
* malware : malware feed related scripts
* osint : osint search related scripts
* yara : yara rules related scripts

**AV:**
* clamav : ClamAV antivirus scripts { * run-av.sh : runs clamav antivirus | * update-av-db.sh : updates a local clamav database }
* f-prot : F-PROT antivirus scripts { * run-av.sh : runs fprot antivirus }
* scan-av.sh : runs a given file through available antivirus programs

**FILE:**
* scan-file.sh : scans a given file for information using local tools file, md5sum, sha256sum, ssdeep, exiftool, floss, olevba, portex

**MALWARE:**
* get-malware.sh : downloads malware feeds (some websites may require a key, if so place a KEY.TXT file inside each subdirectory with the key value)

**OSINT:**
* scan-osint.sh : downloads information from various source feeds (some websites may require a key, if so place a KEY.TXT file inside each subdirectory with the key value) using a .command file containing the command(s) to execute (robtex.com has a python 3 selenium scraper proof of concept search-domain.py)

**YARA:**
* scan-yara.sh : runs yara rules from ./rules on a given file 
* ./rules/update-yara-rules.sh : updates all yara rules downloaded from a git repository